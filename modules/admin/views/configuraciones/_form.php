<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Configuraciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="configuraciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo')->dropDownList([ 'FOOTER_DIRECCION' => 'FOOTER DIRECCION', 'FOOTER_HORARIO' => 'FOOTER HORARIO', 'FOOTER_CORREO' => 'FOOTER CORREO', 'FOOTER_TELEFONOS' => 'FOOTER TELEFONOS', 'FOOTER_FACEBOOK' => 'FOOTER FACEBOOK', 'FOOTER_TWITTER' => 'FOOTER TWITTER', 'FOOTER_INSTAGRAM' => 'FOOTER INSTAGRAM', 'CORREO_CONSULTA' => 'CORREO CONSULTA', 'CONTACTOS_MAPLON' => 'CONTACTOS MAPLON', 'CONTACTOS_MAPLAT' => 'CONTACTOS MAPLAT', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'contenido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechacreacion')->textInput() ?>

    <?= $form->field($model, 'estatus')->dropDownList([ 'ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
