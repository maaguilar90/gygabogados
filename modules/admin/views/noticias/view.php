<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Noticias */

$this->title = $model->idnoticia;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idnoticia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idnoticia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idnoticia',
            'titulo',
            'extracto',
            'contenido',
            'home',
            'orden',
             [
                'attribute'=>'imagen',
                'value'=>'../../img/' .$model->imagen,
                'format' => ['image',['width'=>'200','height'=>'200']],
            ],
            [
                'attribute'=>'imagen2',
                'value'=>'../../img/' .$model->imagen2,
                'format' => ['image',['width'=>'200','height'=>'200']],
            ],
            [
                'attribute'=>'imagen3',
                'value'=>'../../img/' .$model->imagen3,
                'format' => ['image',['width'=>'200','height'=>'200']],
            ],
            'video',
            'estatus',
        ],
    ]) ?>

</div>
