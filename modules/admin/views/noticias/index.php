<?php


use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Noticias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idnoticia',
            'titulo',
            'extracto',
            'contenido',
            'home',
            // 'orden',
            array(
           'attribute' => 'imagen',
            'format' => 'html',
            'value' => function($data) { return Html::img('/web/img/'.$data->imagen, ['width'=>'100']); }),
            array(
           'attribute' => 'imagen2',
            'format' => 'html',
            'value' => function($data) { return Html::img('/web/img/'.$data->imagen2, ['width'=>'100']); }),
            array(
           'attribute' => 'imagen3',
            'format' => 'html',
            'value' => function($data) { return Html::img('/web/img/'.$data->imagen3, ['width'=>'100']); }),
            // 'video',
            // 'estatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
