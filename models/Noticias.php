<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblnoticias".
 *
 * @property integer $idnoticia
 * @property string $titulo
 * @property string $extracto
 * @property string $contenido
 * @property string $home
 * @property integer $orden
 * @property string $imagen
 * @property string $video
 * @property string $estatus
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tblnoticias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'extracto', 'contenido'], 'required'],
            [['home', 'estatus'], 'string'],
            [['orden'], 'integer'],
            [['titulo'], 'string', 'max' => 70],
            [['extracto'], 'string', 'max' => 300],
            [['contenido'], 'string', 'max' => 900],
            [['imagen','imagen2','imagen3', 'video'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idnoticia' => 'Idnoticia',
            'titulo' => 'Titulo',
            'extracto' => 'Extracto',
            'contenido' => 'Contenido',
            'home' => 'Home',
            'orden' => 'Orden',
            'imagen' => 'Imagen',
            'imagen2' => 'Imagen2',
            'imagen3' => 'Imagen3',
            'video' => 'Video',
            'estatus' => 'Estatus',
        ];
    }
}
