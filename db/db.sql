/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.25-MariaDB : Database - dbscare
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tblconfiguraciones` */

DROP TABLE IF EXISTS `tblconfiguraciones`;

CREATE TABLE `tblconfiguraciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` enum('VIDEO','ADSENSE1','ADSENSE2') NOT NULL,
  `contenido` varchar(900) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tblconfiguraciones` */

insert  into `tblconfiguraciones`(`id`,`tipo`,`contenido`,`fechacreacion`,`estatus`) values (1,'VIDEO','https://www.youtube.com/embed/5cWP17jPHW4','2017-07-25 04:57:31','ACTIVO'),(2,'ADSENSE1','<img class=\"\" style=\"z-index:1;    width: 100%;\" src=\"/img/banner_ads.fw.png\">','2017-07-27 01:48:57','ACTIVO'),(3,'ADSENSE2','<img class=\"\" style=\"z-index:1;    width: 100%;\" src=\"/img/banner_ads.fw.png\">','2017-07-27 01:49:04','ACTIVO');

/*Table structure for table `tblmenu` */

DROP TABLE IF EXISTS `tblmenu`;

CREATE TABLE `tblmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) NOT NULL,
  `link` varchar(400) NOT NULL DEFAULT '#',
  `orden` int(2) DEFAULT NULL,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `tblmenu` */

insert  into `tblmenu`(`id`,`menu`,`link`,`orden`,`estatus`) values (1,'OVNIS','#',1,''),(2,'FANTASMAS','#',2,''),(3,'LEYENDAS Y MITOS','#',3,''),(4,'CONSPIRACIONES','#',4,''),(5,'CRIPTOZOOLOGÍA','#',5,''),(6,'RELATOS','#',6,'');

/*Table structure for table `tblnoticias` */

DROP TABLE IF EXISTS `tblnoticias`;

CREATE TABLE `tblnoticias` (
  `idnoticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(70) NOT NULL,
  `extracto` varchar(130) NOT NULL,
  `contenido` varchar(900) NOT NULL,
  `home` enum('SI','NO') NOT NULL DEFAULT 'SI',
  `orden` int(11) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `imagen2` varchar(200) DEFAULT NULL,
  `imagen3` varchar(200) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`idnoticia`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tblnoticias` */

insert  into `tblnoticias`(`idnoticia`,`titulo`,`extracto`,`contenido`,`home`,`orden`,`imagen`,`imagen2`,`imagen3`,`video`,`fechacreacion`,`estatus`) values (1,'Sucesos paranormales Puerto Montt','La noche del domingo 26 de febrero, la gente de puerto Montt fue testigo de hechos paranormales que ','La noche del domingo 26 de febrero, la gente de puerto Montt fue testigo de hechos paranormales que sucedieron en el interior de una casa.','SI',1,'puerto-montt.jpg',NULL,NULL,'','2017-07-25 04:38:46','ACTIVO'),(2,'Extraña Figura en Zambiza','Un extraño fenómeno sucedió en Zambia una figura humanoide apareció en los cielos, dicha figura apareció entre las nubes.','Un extraño fenómeno sucedió en Zambia una figura humanoide apareció en los cielos, dicha figura apareció entre las nubes, media más de 100 metros y su presencia dura aproximadamente 30 minutos según las fuentes.','SI',NULL,'figura_zambia.jpg',NULL,NULL,'','2017-07-25 04:43:16','ACTIVO'),(3,'Señal Alienígena','La noche del domingo 26 de febrero, la gente de puerto Montt fue testigo de hechos paranormales que ','La noche del domingo 26 de febrero, la gente de puerto Montt fue testigo de hechos paranormales que sucedieron en el interior de una casa.','SI',NULL,'senial_alieligena.jpg',NULL,NULL,'','2017-07-25 04:44:15','ACTIVO'),(4,'Inquietante Leyenda del no nacido','Existen varias leyendas urbanas difundidas en internet, una de ellas cuenta la historia de una joven embarazada que vivió una pesa','Existen varias leyendas urbanas difundidas en internet, una de ellas cuenta la historia de una joven embarazada que vivió una pesadilla en su embarazo.','SI',NULL,'no_nacido.jpg',NULL,NULL,NULL,'2017-07-27 01:53:05','ACTIVO'),(5,'Ufo sobre la superficie de la luna ','Un impresionante video que circula en redes sociales muestra un gigantesco ovni que sobrevuela la luna','Un impresionante video que circula en redes sociales muestra un gigantesco ovni que sobrevuela la luna, las imágenes fueron grabadas en directo desde el observatorio el 3 de diciembre del 2016.\r\nLa grabación muestra una sombra de lo que parece ser un ovni en forma de disco, según declaraciones afirman que el objeto no hecho por el hombre por que no se puede observar en la sombra ningúna forma de panel solar o algo parecido a algún satélite enviado por la nasa.\r\nAficionados al tema ufo dicen que es una prueba real de que en el universo no estamos solos.\r\n','SI',NULL,'ovni_luna.jpg',NULL,NULL,NULL,'2017-07-27 01:56:31','ACTIVO'),(6,'Múltiple avistamiento OVNI desconcierta a los habitantes de Inglaterra','Un misterioso objeto volador no identificado ha aparecido en diversas zonas sobre el condado ingles de Cornualles.','Un misterioso objeto volador no identificado ha aparecido en diversas zonas sobre el condado ingles de Cornualles. La gente de todo el condado no ha podido explicar el origen del extraño objeto de color oscuro en los cielos. Ha sido avistado sobre el centro de la ciudad de Truro, en Carluddon, Fistral Beach en Newquay, sobre la autopista inglesa A30, sobre Roche y sobre un surfista que se encontraba en la costa de Cornualles. Después de todos estos avistamientos, machas personas decidieron publicar sus fotos y vídeos en las redes sociales. El primero en verlo fue el surfista Kiefer Krishnan, quien lo grabó en vídeo con su cámara GoPro, y que publicó el lunes en su cuenta de Instagram. El fotógrafo Shayne House también vio el extraño objeto, que parecía desaparecer y rematerialise sobre la autopista inglesa A30, también el lunes.','SI',NULL,'ovni-inglaterra.jpg','ovni-inglaterra2.jpg','','','2017-08-13 12:59:14','ACTIVO'),(7,'Pasajeros aterrorizados dicen que fantasma desapareció delante de tren','Decenas de pasajeros fueron testigos del terrible momento donde una mujer camina delante de tren, pero desaparece frente a ellos','Decenas de pasajeros fueron testigos del terrible momento en que una mujer camina delante de un tren, pero luego desaparece frente a ellos. Aquellos que estuvieron presentes están ahora convencidos de que la mujer es un fantasma ya que no había ni rastro de ella en las vías y nadie la vio marcharse del lugar. os pasajeros esperaban ver el cuerpo de la mujer después de que ella se arrojó delante del tren del servicio de cercanías. Pero sorprendentemente no había ni rastro de ella cuando el tren salió de la estación de Ghatkopar, en Bombay oriental.','SI',NULL,'mujer-tren.jpg','','','','2017-08-13 13:48:39','ACTIVO'),(8,'Fotografían un monstruo similar a Nessie en un lago de Rusia','El monstruo del monstruo del lago Ness parece que ha visitado un lago en Rusia, a más de 6.000 kilómetros de su hogar.','El monstruo del monstruo del lago Ness parece que ha visitado un lago en Rusia, a más de 6.000 kilómetros de su hogar. La imagen, tomada en el lago de Khanto, en Noyabrsk, Rusia central, parece mostrar un cuello largo con una cabeza en el extremo, al igual que la popular imagen de Nessie. También se dice que detrás del enrome “cuello” parece haber una especie de joroba, como las que han aparecido en otras fotografías del monstruo del lago Ness. La gente lleva décadas intentando “cazar” al monstruo en el lago Ness en Escocia.','SI',NULL,'lago-ness.jpg','lago-ness2.jpg','','','2017-08-13 13:54:49','ACTIVO');

/*Table structure for table `tblvisitas` */

DROP TABLE IF EXISTS `tblvisitas`;

CREATE TABLE `tblvisitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idnoticia` int(11) NOT NULL,
  `votos` int(11) NOT NULL DEFAULT '0',
  `fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`idnoticia`,`votos`,`fechacreacion`),
  KEY `idnoticia` (`idnoticia`),
  CONSTRAINT `tblvisitas_ibfk_1` FOREIGN KEY (`idnoticia`) REFERENCES `tblnoticias` (`idnoticia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblvisitas` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `identity` varchar(13) NOT NULL,
  `names` varchar(255) NOT NULL,
  `lastnames` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(9) NOT NULL,
  `cellphone` varchar(10) NOT NULL,
  `sex` enum('MALE','FEMALE') NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('CLIENT','ADMIN') NOT NULL DEFAULT 'CLIENT',
  `auth_key` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'INACTIVE',
  `password_reset_token` varchar(255) DEFAULT NULL,
  `sap_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_has_many` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4491 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`city_id`,`identity`,`names`,`lastnames`,`birthday`,`username`,`password`,`phone`,`cellphone`,`sex`,`creation_date`,`type`,`auth_key`,`status`,`password_reset_token`,`sap_id`) values (1,NULL,'0930178462','Mario','Aguilar','2015-12-02','marioaguilar1990@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','2222222','999999999','MALE','2017-07-09 16:28:02','ADMIN','kmfYgLCF3rgdke9HLPMiVTztN5rOylP6','ACTIVE','s9b0OLmv7cc0Ys5PSkOEW1h0-Hu0_5Dk_1452786912',1107181),(4489,NULL,'9999999999','Ricardo','Aules','0000-00-00','y2rick432@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','2222222','999999999','MALE','2017-07-25 04:50:31','ADMIN',NULL,'ACTIVE',NULL,NULL),(4490,NULL,'9999999998','Marco','Reyes','0000-00-00','marco4482@hotmail.com','827ccb0eea8a706c4c34a16891f84e7b','2222222','999999999','MALE','2017-07-25 04:51:28','ADMIN',NULL,'ACTIVE',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
