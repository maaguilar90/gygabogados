<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\base\ErrorException;
use app\models\User;
use app\models\Noticias;
use app\models\Menu;
use app\models\Slider;
use app\models\Colaboradores;
use app\models\Caracteristicas;
use app\models\Servicios;
use app\models\Configuraciones;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
        'class' => 'yii\web\ErrorAction',
        ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $noticias=  Noticias::find()->where(['estatus'=>'ACTIVO'])->orderBy(['fechacreacion'=>SORT_DESC])->all();
        $menus=  Menu::find()->where(['estatus'=>'ACTIVO'])->orderBy(['orden'=>SORT_ASC])->all();
        $configuraciones= Configuraciones::find()->where(['estatus'=>'ACTIVO'])->all();
        $slider= Slider::find()->where(['estatus'=>'ACTIVO'])->orderBy(['id'=>SORT_ASC])->all();
        $caracteristicas= Caracteristicas::find()->where(['estatus'=>'ACTIVO'])->orderBy(['orden'=>SORT_ASC])->all();//->limit(3);
        $servicios= Servicios::find()->where(['estatus'=>'ACTIVO'])->orderBy(['orden'=>SORT_ASC])->all();
        $colaboradores= Colaboradores::find()->where(['estatus'=>'ACTIVO'])->orderBy(['orden'=>SORT_ASC])->all();
        return $this->render('index',array('noticias'=>$noticias,'menus'=>$menus,'configuraciones'=>$configuraciones,'slider'=>$slider,'caracteristicas'=>$caracteristicas,'servicios'=>$servicios,'colaboradores'=>$colaboradores));
    }

    /**
     * @return string
     */
    public function actionRegistro()
    {
        return $this->render('registro');
    }

    /**
     * @return string
     */
    public function actionQuienessomos()
    {
        return $this->render('quienessomos');
    }

    /**
     * @return string
     */
    public function actionContactanos()
    {
        $configuraciones= Configuraciones::find()->where(['estatus'=>'ACTIVO'])->all();
        return $this->render('contactanos',array('configuraciones'=>$configuraciones));
    }

    /**
     * @return string
     */
    public function actionServicios()
    {
        $servicios= Servicios::find()->where(['estatus'=>'ACTIVO'])->orderBy(['orden'=>SORT_ASC])->all();
        return $this->render('servicios',array('servicios'=>$servicios));
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        $model= New User;
        return $this->render('login',array('model'=>$model));
    }

    public function actionLogout()
    {
    Yii::$app->user->logout();

    return $this->redirect('index');
    }


}
