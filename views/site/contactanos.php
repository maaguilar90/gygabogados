<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Contáctanos';
?>
<section class="site-registro">
<?php foreach ($configuraciones as $key => $value) {
    if ($value->tipo=="FOOTER_DIRECCION"){ $direccion=$value->contenido; }
    if ($value->tipo=="FOOTER_HORARIO"){ $horario=$value->contenido; }
    if ($value->tipo=="FOOTER_CORREO"){ $correo=$value->contenido; }
    if ($value->tipo=="FOOTER_TELEFONOS"){ $telefonos=$value->contenido; }
    if ($value->tipo=="FOOTER_FACEBOOK"){ $facebook=$value->contenido; }
    if ($value->tipo=="FOOTER_TWITTER"){ $twitter=$value->contenido; }
    if ($value->tipo=="FOOTER_INSTAGRAM"){ $instagram=$value->contenido; }
} ?>
	<div>

		<div class="jumbotron">
			<img src="<?= URL::base() ?>/images/secciones/background_contactos.jpg" class="back-seccion">
			<h3><?=$this->title?></h3>
			<p><span style="font-weight: bold; font-size: 14px;">NUESTRA DIRECCIÓN</span></p>
			<p class="seccion-text-c">
				<?=$direccion?>
				<br><span  style="font-weight: bold;">Email: </span><span><?=$correo?></span>
				<br><span  style="font-weight: bold;">Teléfono: </span><span><?=$telefonos?></span>
				
			</p>
			<div class="seccion-text-c">
				<br><div id="map"></div>
			</div>
		</div>

		<div class="body-content">



		</div>
	</div>
<style>
#map {
	height: 400px;

}
</style>
<script>
function initMap() {
	var uluru = {lat: -2.1561944, lng: -79.9126944};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		center: uluru
	});
	var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading">GyGAbogados</h1>'+
            '<div id="bodyContent">'+
            '<p>Somos la firma de Abogados, que cuenta con un equipo de profesionales capacitados en las distintas áreas del derecho.</p>'+
            '<p>Web: <a href="http://web.gygabogados.ec">'+
            'www.gygabogados.ec/</a> '+
            '</p>'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

	var marker = new google.maps.Marker({
		position: uluru,
		map: map,
		title: 'GyGAbogados'
	});
	marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
}
</script>
<style type="text/css">
	.firstHeading
	{
		font-size: 16px !important;
		text-align: center;
	}
	#bodyContent p
	{
		font-size: 13px !important;
	}
	#bodyContent p a
	{
		color: blue !important;
	}
</style>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7kboPTmnBMItOxNAfuf5AOi-WVOPjK4o&callback=initMap"></script>
</section>
