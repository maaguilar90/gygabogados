<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Quienes Somos';
?>
<section class="site-registro">

	<div >

		<div class="jumbotron">
			<img src="<?= URL::base() ?>/images/secciones/background_quienessomos.jpg" class="back-seccion">
			<h3><?=$this->title?></h3>

			<p class="seccion-text">Somos la firma de Abogados, enfocada en ser un estudio jurídico líder en el Ecuador, ofreciendo un servicio integral de calidad en asistencia, defensa, patrocinio, asesoría y consultoría legal, con reconocido prestigio por sus valores y eficacia demostrada, brindando el mejor servicio legal a todos sus clientes, con enfoque social, garantizando la calidad en cada uno de los asuntos judiciales que se ponga en nuestras manos.</p>
		</div>

		<div class="body-content">



		</div>
	</div>
</section>
