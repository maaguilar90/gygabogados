<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'GyGAbogados';

//var_dump($noticias);
$index=0;
$cont=0;
$classActive="active";
?>

<body>
	<div class="">

      <div class="site-wrapper-inner">

        <div class="cover-container">

        	<div class="masthead clearfix">
          <div>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

	              <!-- INDICATORS -->
	              <ol class="carousel-indicators">
	              <?php foreach ($slider as $key => $value) { ?>
	              <?php if ($cont==0){ $styleActive=$classActive; }else{ $styleActive="";  } ?>

	              		<li data-target="#myCarousel" data-slide-to="<?=$cont?>" class="<?=$styleActive?>"></li>
	              <?php $cont++ ?>
	              <?php } ?>
	              </ol>

	              <!-- Wrapper for slides -->
	              <div class="carousel-inner">
	              	<?php $cont=0; ?>
	                <?php foreach ($slider as $key => $value) { $cont++; ?>
	                <?php if ($cont==1){ $styleActive=$classActive; }else{ $styleActive="";  } ?>
	              		<div class="item <?=$styleActive?>">
		                  <img src="<?= URL::base() ?>/images/banners/<?=$value->image?>" alt="<?=$value->alt?>">
		                </div>
	                <?php } ?>
	              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
              </a>

              <div class="form-banner">
                  <div class="content-form">
                    <span class="title">Consulta legal gratuita</span><br>
                    <span class="text">Nos pondremos en contacto para resolver todas<br> tus inquietudes</span><br><br>
                    <input type="text" name="nombres" placeholder="NOMBRES"><br><br>
                    <input type="text" name="mail" placeholder="MAIL"><br><br>
                    <input type="text" name="telefono" placeholder="TELÉFONO"><br><br>
                    <input type="button" class="boton" name="enviar" value="ENVIAR"><br>&nbsp;

                  </div>
              </div>
            </div>
          </div>
          	<!-- Sección Indicadores -->
            <div class="indicadores">
              <div class="content-indicadores">
	        	<?php $cont=0; ?>      
	            <?php foreach ($caracteristicas as $key => $value) { $cont++; ?>
                
                <div class="indicador black" style="background-color:<?=$value->color?>">
                  <img src="<?= URL::base() ?>/images/icon/<?=$value->icono?>"><br><br>
                  <span class="title"><?=$value->titulo?></span><br><br>
                  <span class="contenido"><?=$value->descripcion?></span>

                </div>
	            <?php } ?>

              </div>
            </div>  
              <!-- Fin Sección Indicadores -->

          	    <!-- Sección Servicios -->

                <div class="servicios">
                  <div class="content-servicios">
                    <div class="">
                      <span>NUESTROS SERVICIOS</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                      </div>
                      <div class="dv-servicios">
                      	<?php $cont=0; ?>      
	            		<?php foreach ($servicios as $key => $value) { $cont++; ?>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicios/<?=$value->imagen?>" class="image">
                              <div class="overlay">
                                <div class="text"><?=$value->nombre?></div>
                              </div>
                            </a>
                        </div>
	            		<?php } ?>

                        <!--<div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_consultoria.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_representacion.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_patrocinio.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>-->
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>
                </div>
          	    <!-- Fin Sección Servicios -->
          	    
          	    <!-- Sección Colaboradores -->
	      	    <div class="colaboradores">
	      	    	<div class="content-colaboradores">
	      	    		<span>NUESTROS COLABORADORES</span>
	      	    		<div class="line">
	      	    			<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
	      	    		</div>
	      	    		<?php $cont=0; ?>      
	            		<?php foreach ($colaboradores as $key => $value) { $cont++; ?>
	      	    			<div class="dv-colaboradorint">
	      	    				<div class="img-colabordor">
	      	    					<img src="<?= URL::base() ?>/images/colaboradores/<?=$value->imagen?>" class="">
	      	    				</div>
	      	    				<div class="txt-colaborador">
	      	    					<span class="nombre-col"><?=$value->nombres?></span><br>
	      	    					<span class="titulo-col"><?=$value->titulo?></span><br>
	      	    					<span class="descripcion-col"><?=$value->descripcion ?></span>
	      	    				</div>
	      	    			</div>
	      	    			<!--<div class="dv-colaboradorint">
	      	    				<div class="img-colabordor">
	      	    					<img src="<?//= URL::base() ?>/images/colaboradores/abogado_other.png" class="">
	      	    				</div>
	      	    				<div class="txt-colaborador">
	      	    					<span class="nombre-col">KLEBER GONZALEZ</span><br>
	      	    					<span class="titulo-col">Abogado</span><br>
	      	    					<span class="descripcion-col">Abogado con gran experiencia en asuntos penales</span>
	      	    				</div>
	      	    			</div>-->
	      	    		<?php } ?>
	      	    	</div>
	      	   	</div>

          	    <!-- Fin Sección Colaboradores -->
          	    
          	    <!-- Sección Noticias -->
                <div class="noticias">
                  <div class="content-noticias">
                    <div class="">
                      <span>NOTICIAS</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                      </div>
                      <div class="dv-servicios">
                      	<?php $cont=0; ?>      
	            		<?php foreach ($noticias as $key => $value) { $cont++; ?>
                        <div class="dv-noticiant">
                            <div class="dv-noticiaimg">
                              <a href="">
                                <img src="<?= URL::base() ?>/images/noticias/<?=$value->imagen?>" class="image">
                                <div class="overlay">
                                  <div class="text"></div>
                                </div>
                              </a>
                            </div>
                            <div class="dv-fechanoticia">
                              <span class="fecha-noticia"><?=$value->fechacreacion?></span>
                            </div>
                            <div class="separador-fechanoticia">&nbsp;</div>
                            <a href="">
                              <div class="contenido-noticia">
                                
                                  <span class="tit-noticia"><?=$value->titulo?></span><br>
                                  <span class="txt-noticia"><?=$value->extracto?></span>
                              </div>
                            </a>
                            <div class="separador-noticia">&nbsp;</div>
                        </div>
                        <?php } ?>
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>
              	</div>
          	    <!-- Fin Sección Noticias -->

              



                  </div>

                </div>
            </div>

         




        
  
</body>
</html>