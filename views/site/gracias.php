<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Gracias';
?>
<section class="site-gracias">

    <div >

        <div class="jumbotron">
            <h1><?=$this->title?>!</h1>

            <p><a class="btn btn-lg btn-success" href="/web">HOME</a></p>
        </div>

        <div class="body-content">


        </div>
    </div>
</section>