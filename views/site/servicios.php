<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Servicios';
?>
<section class="site-registro">

	<div >

		<div class="jumbotron">
			<img src="<?= URL::base() ?>/images/secciones/background_servicios.jpg" class="back-seccion">
			<h3><?=$this->title?></h3>

			<p class="seccion-text">Contamos con un equipo de abogados especializados en las diferentes áreas del Derecho, para brindarle un servicio ágil, eficiente y de calidad.</p>
		</div>

		<div class="body-content">

			  <!-- Sección Servicios -->

                <div class="servicios">
                  <div class="content-servicios" style="padding-top:20px;">
                    <div class="">
                      <span>NUESTROS SERVICIOS</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                      </div>
                      <div class="dv-servicios">
                      	<?php $cont=0; ?>      
	            		<?php foreach ($servicios as $key => $value) { $cont++; ?>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicios/<?=$value->imagen?>" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
	            		<?php } ?>

                        <!--<div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_consultoria.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_representacion.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="<?= URL::base() ?>/images/servicio_patrocinio.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>-->
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>
                </div>
          	    <!-- Fin Sección Servicios -->

		</div>
	</div>
</section>
