<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/site.css" rel="stylesheet" media="screen">
    <link href="css/carousel.css" rel="stylesheet" media="screen">
    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

	<title>G&G Abogados</title>
</head>
<body>
	<div class="">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">
              <img src="img/logo.png" alt="logo" class="logo"></h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li><a href="/">Inicio</a></li>
                  <li class="active"><a href="#">Quienes Somos</a></li>
                  <li><a href="servicios.php">Servicios</a></li>
                  <li><a href="noticias.php">Noticias</a></li>
                  <li><a href="#">Contáctanos</a></li>
                  <li><a href="#">Mi Cuenta</a></li>
                </ul>
              </nav>
            </div>

            <div class="indicadores">
              <img src="img/quienes-somos.jpg" class="banner-seccion">

               <div class="content">
                  <div class="content-seccion">
                    <div class="">
                      <span class="tit-seccion">QUIENES SOMOS</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                      </div>
                      <div class="dv-seccion">
                        Somos la firma de Abogados, que cuenta con un equipo de profesionales capacitados en las distintas áreas del derecho. Los cuales lo podrán asesorar en sus problemas y trámites judiciales.​
 
Le brindamos el mejor servicio judicial. Solo separe su consulta y lo atenderemos gustosos. 
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>

                


                 <div class="colaboradores">
                    <div class="content-colaboradores">
                    <span>NUESTROS COLABORADORES</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                      </div>
                      <div class="dv-colaboradorint">
                        <div class="img-colabordor">
                          <img src="img/abogado_giovani.png" class="">
                        </div>
                        <div class="txt-colaborador">
                          <span class="nombre-col">GEOVANNI MAYORGA</span><br>
                          <span class="titulo-col">Abogado</span><br>
                          <span class="descripcion-col">Abogado con gran experiencia en varios campos</span>
                        </div>
                      </div>
                      <div class="dv-colaboradorint">
                        <div class="img-colabordor">
                          <img src="img/abogado_other.png" class="">
                        </div>
                        <div class="txt-colaborador">
                          <span class="nombre-col">KLEBER GONZALEZ</span><br>
                          <span class="titulo-col">Abogado</span><br>
                          <span class="descripcion-col">Abogado con gran experiencia en asuntos penales</span>
                        </div>
                      </div>
                    </div>

                    <div class="servicios">
                  <div class="content-servicios">
                    <div class="">
                      <span>SERVICIOS</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                      </div>
                      <div class="dv-servicios">
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="img/servicio_defensa.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="img/servicio_consultoria.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="img/servicio_representacion.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                        <div class="dv-serviciont">
                            <a href="">
                              <img src="img/servicio_patrocinio.jpg" class="image">
                              <div class="overlay">
                                <div class="text"></div>
                              </div>
                            </a>
                        </div>
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>

                    <br><br>

                  <div class="footer">

                    <div class="content-footer">
                      <div class="columna-footer">
                        <img src="img/logo2.png" alt="logo" class="logo">
                      </div>
                      <div class="columna-footer">
                        <div class="columna1">
                          <li>Inicio</li><br>
                          <li>Noticias</li>
                        </div>
                        <div class="columna2">
                          <li>Quienes Somos</li><br>
                          <li>Contáctanos</li>
                        </div>
                      </div>
                      <div class="columna-footer">
                          <div class="dir-footer"><img src="img/ico-ubicacion.png">&nbsp;&nbsp;Cdla. Urbanor, diagonal a la Gasolinera de la Av. Las Aguas</div>
                          <div class="ate-footer"><img src="img/ico-atencion.png">&nbsp;&nbsp;Lunes a Viernes 9:00 - 18:00</div>
                          <div class="mail-footer"><img src="img/ico-mail.png">&nbsp;&nbsp;gglegales@hotmail.com</div>
                          <div class="tel-footer"><img src="img/ico-telefono.png">&nbsp;&nbsp;(593) 0986313786 - (593) 097 943 4399</div>
                      </div>  
                    </div>

                  </div>




                  </div>

                </div>
            </div>

         




          </div>

          </div>

        </div> 	

      </div>

     

    </div>
  
</body>
</html>