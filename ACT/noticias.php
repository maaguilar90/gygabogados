<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/site.css" rel="stylesheet" media="screen">
    <link href="css/carousel.css" rel="stylesheet" media="screen">
    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

	<title>G&G Abogados</title>
</head>
<body>
	<div class="">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">
              <img src="img/logo.png" alt="logo" class="logo"></h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li><a href="/">Inicio</a></li>
                  <li><a href="quienessomos.php">Quienes Somos</a></li>
                  <li><a href="servicios.php">Servicios</a></li>
                  <li class="active"><a href="#">Noticias</a></li>
                  <li><a href="#">Contáctanos</a></li>
                  <li><a href="#">Mi Cuenta</a></li>
                </ul>
              </nav>
            </div>

            <div class="indicadores">
              <img src="img/noticias.jpg" class="banner-seccion">

               <div class="content">
                  <div class="content-seccion">
                    <div class="">
                      <span class="tit-seccion">NOTICIAS</span>
                      <div class="line">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                      </div>
                      <div class="dv-seccion">
                       Este espacio fue creado con la finalidad de que nuestros profesionales realicen apuntes académicos para aportar con sus conocimientos en estas Notas Jurídicas, los cuales están disponibles para todos los usuarios que visiten la página de G&G Abogados.
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>

                <div class="noticias">
                  <div class="content-noticias"  style="padding-top: 1%;">
                      <div class="dv-servicios">
                        <div class="dv-noticiant">
                            <div class="dv-noticiaimg">
                              <a href="">
                                <img src="img/noticia_aborto.jpg" class="image">
                                <div class="overlay">
                                  <div class="text"></div>
                                </div>
                              </a>
                            </div>
                            <div class="dv-fechanoticia">
                              <span class="fecha-noticia">15 de Agosto del 2017</span>
                            </div>
                            <div class="separador-fechanoticia">&nbsp;</div>
                            <a href="">
                              <div class="contenido-noticia">
                                
                                  <span class="tit-noticia">¿Existe Estado de Necesidad en el Aborto?</span><br>
                                  <span class="txt-noticia">De acuerdo a nuestra legislación se ha determinado el Estado de Necesidad, como un estado de la actividad anti-jurídica que debe contener tres elementos constitutivos</span>
                              </div>
                            </a>
                            <div class="separador-noticia">&nbsp;</div>
                        </div>
                         <div class="dv-noticiant">
                            <div class="dv-noticiaimg">
                              <a href="">
                                <img src="img/noticia_indulto.jpg" class="image">
                                <div class="overlay">
                                  <div class="text"></div>
                                </div>
                              </a>
                            </div>
                            <div class="dv-fechanoticia">
                              <span class="fecha-noticia">10 de Junio del 2017</span>
                            </div>
                            <div class="separador-fechanoticia">&nbsp;</div>
                            <a href="">
                              <div class="contenido-noticia">
                                
                                  <span class="tit-noticia">Indulto</span><br>
                                  <span class="txt-noticia">El indulto puede definirse de manera fugaz como una medida de gracia que el poder otorga a los condenados con sentencia firme, levantando la pena que se les hubiera impuesto o parte de ella, y cambiandola por otra más baja</span>
                              </div>
                            </a>
                            <div class="separador-noticia">&nbsp;</div>
                        </div>

                         <div class="dv-noticiant">
                            <div class="dv-noticiaimg">
                              <a href="">
                                <img src="img/noticia_declaracion.jpg" class="image">
                                <div class="overlay">
                                  <div class="text"></div>
                                </div>
                              </a>
                            </div>
                            <div class="dv-fechanoticia">
                              <span class="fecha-noticia">7 de Diciembre del 2016</span>
                            </div>
                            <div class="separador-fechanoticia">&nbsp;</div>
                            <a href="">
                              <div class="contenido-noticia">
                                
                                  <span class="tit-noticia">Declaración de Parte COGEP</span><br>
                                  <span class="txt-noticia">Al leer o escuchar este término, nos trae al imaginario de nuestra mente la figura de un testigo frente al Juez; pues bien, si no les trajo a la mente esta escena entonces les mencionaré que antes del COGEP</span>
                              </div>
                            </a>
                            <div class="separador-noticia">&nbsp;</div>
                        </div>

                         <div class="dv-noticiant">
                            <div class="dv-noticiaimg">
                              <a href="">
                                <img src="img/noticia_derecho.jpg" class="image">
                                <div class="overlay">
                                  <div class="text"></div>
                                </div>
                              </a>
                            </div>
                            <div class="dv-fechanoticia">
                              <span class="fecha-noticia">6 de Octubre del 2016</span>
                            </div>
                            <div class="separador-fechanoticia">&nbsp;</div>
                            <a href="">
                              <div class="contenido-noticia">
                                
                                  <span class="tit-noticia">El Derecho</span><br>
                                  <span class="txt-noticia">El derecho, es un término que ha venido siendo la máxima referencia humana sobre su ser, como parte activa en la sociedad, el derecho es un concepto intangible de lo bueno y lo malo tal como el hombre lo percibía desde los inicios de la civilización</span>
                              </div>
                            </a>
                            <div class="separador-noticia">&nbsp;</div>
                        </div>
                        
                      </div>
                      <div>
                        <input type="button" class="boton" name="enviar" value="ENVIAR" style="display:none;"><br>&nbsp;
                      </div>
                    </div>
                  </div>


                 

                    
                    <br><br>

                  <div class="footer">

                    <div class="content-footer">
                      <div class="columna-footer">
                        <img src="img/logo2.png" alt="logo" class="logo">
                      </div>
                      <div class="columna-footer">
                        <div class="columna1">
                          <li>Inicio</li><br>
                          <li>Noticias</li>
                        </div>
                        <div class="columna2">
                          <li>Quienes Somos</li><br>
                          <li>Contáctanos</li>
                        </div>
                      </div>
                      <div class="columna-footer">
                          <div class="dir-footer"><img src="img/ico-ubicacion.png">&nbsp;&nbsp;Cdla. Urbanor, diagonal a la Gasolinera de la Av. Las Aguas</div>
                          <div class="ate-footer"><img src="img/ico-atencion.png">&nbsp;&nbsp;Lunes a Viernes 9:00 - 18:00</div>
                          <div class="mail-footer"><img src="img/ico-mail.png">&nbsp;&nbsp;gglegales@hotmail.com</div>
                          <div class="tel-footer"><img src="img/ico-telefono.png">&nbsp;&nbsp;(593) 0986313786 - (593) 097 943 4399</div>
                      </div>  
                    </div>

                  </div>




                  </div>

                </div>
            </div>

         




          </div>

          </div>

        </div> 	

      </div>

     

    </div>
  
</body>
</html>